require "lib.slam"
vector = require "lib.hump.vector"
tlfres = require "lib.tlfres"

require "helpers"

CANVAS_WIDTH = 1920
CANVAS_HEIGHT = 1080

GRID_WIDTH = 12
GRID_HEIGHT = 8
TILE_SIZE = 100
TILE_DURABILITY = 5

MAX_LOOT_NUMBER = 5

player = {x=0,y=0,r=0}
home = {x=0,y=0}

original_duration = 1
turn_duration = original_duration
remaining_time_on_lava = turn_duration

original_duration_stone = 3
remaining_time_on_stone = original_duration_stone

loot = {}

savedscore = 0
score = 0
state = "title"
at_home = true

f = 100/16

intro_text_1 = "I may be a small lizard, but I love sparkly gems just as much as the great old dragons :)"
intro_text_2 = "The gems help protect me from the deadly heat of lava, so I want to bring lots of them into my nest."
intro_text_3 = "But I have to be quick and careful, because the floor is breaking away under my feet..."
intro_text_4 = "(press any key to start playing)"

function love.load()
    -- set up default drawing options
    love.graphics.setDefaultFilter( "nearest", "nearest", 1 )
    love.graphics.setBackgroundColor(0, 0, 0)

    -- load assets
    images = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("images")) do
        if filename ~= ".gitkeep" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/"..filename)
        end
    end

    sounds = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
        end
    end
    sounds.die:setVolume(0.7)
    sounds.pickup:setVolume(0.5)
    sounds.gem_break:setVolume(0.55)
    for i=1,5 do
        sounds["break"..i]:setVolume(0.2)
    end

    music = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename, "stream")
            music[filename:sub(1,-5)]:setLooping(true)
        end
    end
    music.blub:setPitch(0.6)
    music.blub:setVolume(0.25)

    fonts = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("fonts")) do
        if filename ~= ".gitkeep" then
            fonts[filename:sub(1,-5)] = {}
            for fontsize=100,200 do
                fonts[filename:sub(1,-5)][fontsize] = love.graphics.newFont("fonts/"..filename, fontsize)
            end
        end
    end
    love.graphics.setFont(fonts["m5x7"][100])
    --initialize particles
    pAcc = 1000 --acceleration
    psystem = love.graphics.newParticleSystem(images.particle, 1024)
	psystem:setSizes(2*f)
    psystem:setColors(1, 1, 1, 1, 0.3, 0.6, 1, 0) -- white, fade to pale blue transparency
    psystem:setEmissionArea("uniform", 10, 10)
	psystem:setParticleLifetime(0.2, 0.3)
	psystem:setSpeed(350, 400)
	psystem:setSpread(2*math.pi)

    --start the game and generate a level
    restart()
end

function restart()
    psystem:reset() --clear particles from previous round
    if state == "gameover" then
        state = "game"
        music.blub:play()
    elseif state == "game" or state == "intro" then
        -- don't change the state
    else --when the program is first opened
        state = "title"
    end
    --initialize grid
    grid = {}
    for x=1,GRID_WIDTH do
        table.insert(grid, {})
        for y=1,GRID_HEIGHT do
            v = love.math.random(0,TILE_DURABILITY-1)+love.math.random(0,1)
            r = love.math.random(0, 3)
            table.insert(grid[x], {value=v, rotation=r})
        end
    end
    --print("grid initialized")
    spawn_home_and_player() --player at random start position
    --print("player and home position initialized")
    score = 0 --reset the score
    
    --initialize loot positions
    loot = {}
    free_space = count()
    for l=1, math.min(MAX_LOOT_NUMBER, free_space) do
        table.insert(loot,{x=0,y=0})
    end
    --print("loot number: "..#loot)
    for l=1,#loot do
        loot[l].x, loot[l].y = getRandomFreeTile()
    end
    --print("loot positions initialized")
end

function getRandomFreeTile()
    if count() <=1 then
        state = "gameover"
        return nil
    end
    ok = false
    while not ok do
        x = love.math.random(1, GRID_WIDTH)
        y = love.math.random(1, GRID_HEIGHT)
        if tile(x, y) then
            ok = true
        end
        if player.x == x and player.y == y then
            ok = false
        end
        for l=1,#loot do
            if loot[l].x == x and loot[l].y == y then
                ok = false
            end
        end
    end
    return x,y
end

function love.update(dt)
    if state == "game" then
        if grid[player.x][player.y].value == 0 and not at_home then -- if standing on lava
            remaining_time_on_lava = remaining_time_on_lava - dt
            if remaining_time_on_lava <= 0 then
                state = "gameover"
                music.blub:stop()
                sounds.die:play()
            end
        elseif tile(player.x, player.y) and not at_home then --if standing on stone
            remaining_time_on_stone = remaining_time_on_stone - dt
            if remaining_time_on_stone <= 0 then
                crack()
                remaining_time_on_stone = original_duration_stone
            end
        end
    --update the particles
    psystem:update(dt)
    end
end

--returns the number of non-lava tiles
function count()
    c = 0
    for x=1,GRID_WIDTH do
        for y=1,GRID_HEIGHT do
            if tile(x, y) then
                c = c+1
            end
        end
    end
    return c
end

function crack()
    if tile(player.x, player.y) then
        --play sound:
        i = grid[player.x][player.y].value
        sounds["break"..i]:setPitch(math.random(150, 170)/100)
        love.audio.play(sounds["break"..i])
        --decrement tile value:
        grid[player.x][player.y].value = grid[player.x][player.y].value - 1
        --restart stone timer:
        remaining_time_on_stone = original_duration_stone 
    end
end

function spawn_home_and_player()
    if count() > 1 then
        home.x,home.y = getRandomFreeTile()
        player.x,player.y = home.x,home.y
        remaining_time_on_lava = turn_duration
    else
        state = "gameover"
    end
end

function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

--find out if there is a floor tile
function tile(x, y)
    return grid[x] and grid[x][y] and grid[x][y].value > 0
end

function love.keypressed(key)
    moved = false
    -- global keys
    if (key == "escape" and state == "credits") then
        love.window.setFullscreen(false)
        love.event.quit()
    elseif key == "escape" then
        state = "credits"
        music.blub:stop()
        return
    elseif key == "r" then
        restart()
        sounds.done:play()
    elseif key == "f" then
        isFullscreen = love.window.getFullscreen()
        love.window.setFullscreen(not isFullscreen)
    end    
    -- states where any key will do the same
    if state == "title" then
        state = "intro"
        return
    elseif state == "intro" then
        state = "game"
        music.blub:play()
        return
    end
    -- keys for game state only
    if state == "game" then
        if key == "up" then
            player.y = player.y-1
            moved = true
            player.r = 0
        elseif key == "down" then
            player.y = player.y+1
            moved = true
            player.r = 2
        elseif key == "left" then
            player.x = player.x-1
            moved = true
            player.r = 3
        elseif key == "right" then
            player.x = player.x+1
            moved = true
            player.r = 1
        elseif key == "return" and at_home then
            if score > savedscore then
                savedscore = score
            end
            sounds.done:play()
            restart()
        end
        
        -- do not move out ouf the playing field
        if player.x > GRID_WIDTH then
            player.x = GRID_WIDTH
            moved = false
        end
        if player.x < 1 then
            player.x = 1
            moved = false
        end
        if player.y > GRID_HEIGHT then
            player.y = GRID_HEIGHT
            moved = false
        end
        if player.y < 1 then
            player.y = 1
            moved = false
        end
        
        -- if player is at home, do home stuff
        if (player.x == home.x and player.y == home.y) then
            at_home = true
        else
            at_home = false
        end
        
        -- if the player has moved
        if moved then
            remaining_time_on_lava = turn_duration --restart lava timer
            -- on lava, consume the gems
            if not (tile(player.x, player.y) or at_home) then
                score = score - 1
                --play sound:
                sounds.gem_break:play()
                --set particle position:
                psystem:moveTo(player.x*TILE_SIZE, player.y*TILE_SIZE)
                --spawn particles:
                psystem:emit(64)
                
                if score < 0 then
                    score = 0 --reset score so it will not be negative
                    state = "gameover"
                    music.blub:stop()
                    sounds.die:play()
                end
            -- on tiles, crack the stone
            else
                crack()
            end
        end
        
        -- find out if player has stepped on a gem
        for l=1,#loot do
            if (player.x == loot[l].x and player.y == loot[l].y) then
                pickupLoot(l)
                break
            end
        end
    end
end

function pickupLoot(lootnr)
    --play sound
    sounds.pickup:setPitch(math.random(100, 105)/100)
    love.audio.play(sounds.pickup)
    --increase score
    score = score + 1
    --new loot position:
    if count() > #loot then
        loot[lootnr].x, loot[lootnr].y = getRandomFreeTile()
    else
        table.remove(loot, lootnr)
    end
end

function love.keyreleased(key)
end

function love.mousepressed(x, y, button)
end

function love.draw()
    love.graphics.setColor(255, 255, 255, 255)
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

    -- Draw the game here!

    if state == "gameover" then
        love.graphics.setColor(1, 0.6, 0)
        love.graphics.printf("GAME OVER :(", 0, 3*TILE_SIZE, CANVAS_WIDTH, "center")
        love.graphics.setColor(0.5, 0.7, 1)
        love.graphics.printf("Your score: "..score, 0, 5*TILE_SIZE, CANVAS_WIDTH, "center")
        love.graphics.setColor(0.5, 0.5, 0.5)
        love.graphics.printf("Press R to restart or ESC to quit.", 0, 7*TILE_SIZE, CANVAS_WIDTH, "center")
    elseif state == "credits" then
        drawCredits()
    elseif state == "title" then
        drawTitle()
    elseif state == "intro" then
        drawIntro()
    elseif state == "game" then
    
        gridOffsetX = 0.5*TILE_SIZE
        gridOffsetY = gridOffsetX
        -- draw lava
        love.graphics.setColor(1, 1, 1)
        for x=0, ((GRID_WIDTH-1)/4) do
            for y=0, ((GRID_HEIGHT-1)/4) do
                love.graphics.draw(images.lava_large, (x*TILE_SIZE*4)+gridOffsetX, (y*TILE_SIZE*4)+gridOffsetY, 0, f, f)
            end
        end
        -- draw stone floor
        for x=1,GRID_WIDTH do
            for y=1,GRID_HEIGHT do
                if tile(x,y) then
                    v = grid[x][y].value
                    r = grid[x][y].rotation
                    rotate = r*(math.pi/2)
                    offset = images.stone1:getWidth()/2
                    love.graphics.draw(images["stone"..v], ((x-0.5)*TILE_SIZE)+gridOffsetX, ((y-0.5)*TILE_SIZE)+gridOffsetY, rotate, f, f, offset, offset)
                end
            end
        end
    
        --draw loot
        love.graphics.setColor(1, 1, 1)
        for l=1,#loot do
            love.graphics.draw(images.crystal_small, ((loot[l].x-1)*TILE_SIZE)+gridOffsetX, ((loot[l].y-1)*TILE_SIZE)+gridOffsetY, 0, f, f)
        end
        --draw home
        love.graphics.draw(images.home, ((home.x-0.5)*TILE_SIZE)+gridOffsetX, ((home.y-0.5)*TILE_SIZE)+gridOffsetY, 0, f, f, offset, offset)
        
        --draw particles
        love.graphics.draw(psystem, 0, 0)
        
        --draw player
        love.graphics.setColor(1, 1, 1)
        rotate = player.r*(math.pi/2)
        offset = images.lizard:getWidth()/2
        love.graphics.draw(images.lizard, (((player.x)-0.5)*TILE_SIZE)+gridOffsetX, (((player.y)-0.5)*TILE_SIZE)+gridOffsetY, rotate, f, f, offset, offset)

        --draw time bar
        love.graphics.setColor(0.1, 0.1, 0.1)
        h = 50
        w = GRID_WIDTH*TILE_SIZE
        yPos = gridOffsetY+GRID_HEIGHT*TILE_SIZE+h
        love.graphics.rectangle("fill", gridOffsetX, yPos, w, h)
        rw = turn_duration*w
        if not (tile(player.x, player.y) or at_home) then
            rw = (remaining_time_on_lava/turn_duration)*w
        elseif (tile(player.x, player.y) and not at_home) then
            rw = (remaining_time_on_stone/original_duration_stone)*w
        end
        love.graphics.setColor(0.9, 0.5, 0.1)
        love.graphics.rectangle("fill", gridOffsetX, yPos, rw, h)
        
        --draw score & info texts
        textPosX = (GRID_WIDTH+1)*TILE_SIZE
        if score == 1 then
            tGems = "gem."
        else
            tGems = "gems."
        end
        love.graphics.setColor(0.5, 0.7, 1)
        love.graphics.print("You have "..score.." "..tGems, textPosX, gridOffsetY)
        if at_home then    
            texts = {
                    "Press ENTER to",
                    "save your score",
                    "and restart."
                    }
            for i = 1,#texts do
                love.graphics.print(texts[i], textPosX, gridOffsetY+((i+1)*TILE_SIZE))
            end
            
        end
        if savedscore == 1 then
                tGems = "gem"
            else
                tGems = "gems"
            end
        love.graphics.setColor(0.5, 0.5, 0.5)
        love.graphics.print("Your best score:", textPosX, 6.5*TILE_SIZE)
        love.graphics.print( savedscore.." "..tGems, textPosX, 7.5*TILE_SIZE)
        love.graphics.setColor(0.5, 0.5, 0.5)
        love.graphics.printf( "ARROW KEYS: move    R: restart    ESC: quit", gridOffsetX, 9.7*TILE_SIZE, 18.5*TILE_SIZE, "center")
        
    end

    tlfres.endRendering()
end

function drawIntro()
    love.graphics.setColor(0.5, 0.7, 1)
    love.graphics.printf(intro_text_1, TILE_SIZE, TILE_SIZE, 13*TILE_SIZE, "left")
    love.graphics.printf(intro_text_2, TILE_SIZE, 4*TILE_SIZE, 13*TILE_SIZE, "left")
    love.graphics.printf(intro_text_3, TILE_SIZE, 7*TILE_SIZE, 18*TILE_SIZE, "left")
    love.graphics.setColor(0.5, 0.5, 0.5)
    love.graphics.printf(intro_text_4, TILE_SIZE, 9*TILE_SIZE, 18*TILE_SIZE, "right")
    love.graphics.setColor(1, 1, 1)
    --lava background
    love.graphics.draw(images.lava_large, 14.5*TILE_SIZE, 1.5*TILE_SIZE, 0, f, f)
    offset = images.stone1:getWidth()/2
    --first row of tiles
    love.graphics.draw(images.stone4, 15*TILE_SIZE, 2*TILE_SIZE, (math.pi/2), f, f, offset, offset)
    love.graphics.draw(images.stone5, 16*TILE_SIZE, 2*TILE_SIZE, 0, f, f, offset, offset)
    love.graphics.draw(images.stone5, 17*TILE_SIZE, 2*TILE_SIZE, 0, f, f, offset, offset)
    love.graphics.draw(images.stone5, 18*TILE_SIZE, 2*TILE_SIZE, 0, f, f, offset, offset)
    love.graphics.draw(images.home, 18*TILE_SIZE, 2*TILE_SIZE, 0, f, f, offset, offset)
    --second row of tiles
    love.graphics.draw(images.stone3, 15*TILE_SIZE, 3*TILE_SIZE, 0, f, f, offset, offset)
    love.graphics.draw(images.stone4, 16*TILE_SIZE, 3*TILE_SIZE, 0, f, f, offset, offset)
    love.graphics.draw(images.stone5, 17*TILE_SIZE, 3*TILE_SIZE, 0, f, f, offset, offset)
    love.graphics.draw(images.lizard, 17*TILE_SIZE, 3*TILE_SIZE, 3*(math.pi/2), f, f, offset-1, offset-1)
    love.graphics.draw(images.stone5, 18*TILE_SIZE, 3*TILE_SIZE, 0, f, f, offset, offset)
    --third row of tiles
    love.graphics.draw(images.stone1, 15*TILE_SIZE, 4*TILE_SIZE, 0, f, f, offset, offset)
    love.graphics.draw(images.stone2, 16*TILE_SIZE, 4*TILE_SIZE, 0, f, f, offset, offset)
    love.graphics.draw(images.stone3, 17*TILE_SIZE, 4*TILE_SIZE, 2*(math.pi/2), f, f, offset, offset)
    love.graphics.draw(images.stone4, 18*TILE_SIZE, 4*TILE_SIZE, 3*(math.pi/2), f, f, offset, offset)
    --fourth row of tiles
    --(empty tile)
    love.graphics.draw(images.stone1, 16*TILE_SIZE, 5*TILE_SIZE, 2*(math.pi/2), f, f, offset, offset)
    love.graphics.draw(images.stone2, 17*TILE_SIZE, 5*TILE_SIZE, (math.pi/2), f, f, offset, offset)
    love.graphics.draw(images.stone3, 18*TILE_SIZE, 5*TILE_SIZE, 0, f, f, offset, offset)
    --gems
    love.graphics.draw(images.crystal_small, 16*TILE_SIZE, 2*TILE_SIZE, 0, f, f, offset, offset)
    love.graphics.draw(images.crystal_small, 17*TILE_SIZE, 5*TILE_SIZE, 0, f, f, offset, offset)
end

function drawTitle()
    --text
    love.graphics.setColor(1, 0.6, 0)
    love.graphics.setFont(fonts["m5x7"][200])
    love.graphics.printf("THE FLOOR IS", 1.5*TILE_SIZE, 0.5*TILE_SIZE, 16*TILE_SIZE, "center")
    love.graphics.setColor(0.5, 0.7, 1)
    love.graphics.setFont(fonts["m5x7"][100])
    love.graphics.printf("a game made by Kiki and blinry", 1.5*TILE_SIZE, 8.5*TILE_SIZE, 16*TILE_SIZE, "center")
    love.graphics.printf("inspired by onehourgamejam #196", 1.5*TILE_SIZE, 9.5*TILE_SIZE, 16*TILE_SIZE, "center")
    love.graphics.setColor(1, 1, 1)
    --decorative items
    love.graphics.draw(images.loot, 3*TILE_SIZE, TILE_SIZE, 0, f, f)
    love.graphics.draw(images.player, 15*TILE_SIZE, TILE_SIZE, 0, f, f)
    --lava background
    love.graphics.draw(images.lava_large, 1.5*TILE_SIZE, 3*TILE_SIZE, 0, f, f)
    love.graphics.draw(images.lava_large, 5.5*TILE_SIZE, 3*TILE_SIZE, 0, f, f)
    love.graphics.draw(images.lava_large, 9.5*TILE_SIZE, 3*TILE_SIZE, 0, f, f)
    love.graphics.draw(images.lava_large, 13.5*TILE_SIZE, 3*TILE_SIZE, 0, f, f)
    --stone cover
    offset = images.stone1:getWidth()/2
    titlestones = {{1,3,4,8,10,12,16},
                    {1,3,4,6,8,10,12,14,16},
                    {1,3,4,8,10,12,16},
                    {1,4,6,8,12,14,16}}
    for i=1,#titlestones do
        for j=1,#(titlestones[i]) do
            love.graphics.draw(images.stone5, (1+titlestones[i][j])*TILE_SIZE, (2.5+i)*TILE_SIZE, 0, f, f, offset, offset)
        end
    end
    titlestones2 = {{5,7,13,15},
                    {},
                    {},
                    {9,9,11,11}}
    titlestones2r = {{2,3,2,3},
                    {},
                    {},
                    {2,1,3,0}}
    for i=1,#titlestones2 do
        for j=1,#(titlestones2[i]) do
            love.graphics.draw(images.stone5, (1+titlestones2[i][j])*TILE_SIZE, (2.5+i)*TILE_SIZE, titlestones2r[i][j]*(math.pi/2), f, f, offset-5, offset-5)
        end
    end
end

function drawCredits()
    --text:
    love.graphics.setColor(1, 0.6, 0)
    love.graphics.setFont(fonts["m5x7"][200])
    love.graphics.printf("Thank you for playing!", 1.5*TILE_SIZE, 1*TILE_SIZE, 16*TILE_SIZE, "center")
    love.graphics.setColor(0.5, 0.7, 1)
    love.graphics.setFont(fonts["m5x7"][100])
    love.graphics.printf("You can find more games on our websites :)", 1.5*TILE_SIZE, 4*TILE_SIZE, 16*TILE_SIZE, "center")
    love.graphics.printf("Kiki:  metakiki.net", 1.5*TILE_SIZE, 6*TILE_SIZE, 16*TILE_SIZE, "center")
    love.graphics.printf("blinry:  morr.cc", 1.5*TILE_SIZE, 7*TILE_SIZE, 16*TILE_SIZE, "center")
    love.graphics.setColor(1, 1, 1)
    --portraits:
    love.graphics.draw(images.pixelkiki, 10*TILE_SIZE, 8.5*TILE_SIZE, 0, f, f)
    love.graphics.draw(images.pixelblinry, 8*TILE_SIZE, 8.5*TILE_SIZE, 0, f, f)
    --decorative items:
    --love.graphics.draw(images.loot, 0.5*TILE_SIZE, 1.5*TILE_SIZE, 0, f, f)
    --love.graphics.draw(images.player, 17.5*TILE_SIZE, 1.5*TILE_SIZE, 0, f, f)
end







